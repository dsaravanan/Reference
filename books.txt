Para Annual|2009|Issue 1 
A CMI Literature club initiative

Book: Book of Job
Author: Unknown

Book: A Lesson Before Dying
Author: Earnest J. Gaines

Book: City of Djinns
Author: William Dalrymple

Book: Reading Lolita in Tehran
Author: Azar Nafisi

Book: Small Island
Author: Andrea Levy

Book: Joothan
Author: Om Prakash Valmiki

Book: The Handmaid's Tale
Author: Margaret Atwood

Book: The Bridge of San Luis Rey
Author: Thornton Wilder

Book: Lincoln in the Bardo
Author: George Sanders

Book: Colourless Tsukuru Tazaki and His Years of Pilgrimage
Author: Haruki Murakami

Book: A Perfect day for Bananafish, 
      The Laughing Man and
      For Esme-with Love and Squalor

Book: Human Action
Author: Von Mise

Book: 1984
Author: George Orwell

Book: The Joyous Cosmology
Author: Alan Watts

Book: Tuesdays with Morrie
Author: Mitch Albom

Book: Salvation of a Saint
Author: Keigo Higashino

Book: Ponniyin Selvan
Author: Kalki Krishnamurthy

Book: City of Thieves
Author: David Benioff

Book: Human Action: A Treatise on Economics
Author: Ludwig von Mises

Book: The Emperor of All Maladies: A Biography of Cancer
Author: Siddhartha Mukherje

Book: Learning Python with Raspberry Pi
Author: Alex Bradbury and Ben Everard

# Books List
Notes on Linear Algebra Peter S. Comeron
Education in Ancient India Dr. A. S. Altekar
Universites in Ancient India D.G. Apte
2000 BC - 300 AD R.C.Dutt
A Fragmentary Tale of the Atom
The Computing Legacy of Alan M. Turing
Ancient India S. Krishnasward A. Iyengar
Computing Machinary and Intelligence
On Computable Numbers, with an application to the Entscheideng and problem
The Chemical Basics of Morphogenesis
Notes on Finite Group Theory
How to learn from experience R. Adhikari and R. Siddharthan
How to Prove It (A Structured Approach) Daniel J. Velleman
Vector Calculus (Second Edition) Susan Jane Colley
All the Math you missed (Second Edition) Thomas A. Garrity
Advanced Engineering Mathematics (Second Edition) Erwin Kreyszig
Introduction to Probability and Statistics for Enginners and Scientists (Third Edition) Sheldon M. Ross
Analytic Geometry V.A. Ilyin and E.G. Poznyak
Analysis I (Third Edition) Terence Tao
The theory of substitutions and its applications to algebra Eugen Netto
Introduction to Topology (Second Edition) Bert Mendelson
Mathematical Proofs: A Transition to Advanced Mathematics (4th Edition) Gary Chartrand, Albert Polimeni, Ping Zhang
A Hilbert Space Problem Book Paul R. Halmos
I want to be a Mathematician: An Automathography P.R. Halmos
A Mathematician's Apology G.H. Hardy
Naive Set Theory Paul R. Halmos
Finite-Dimensional Vector Spaces (2nd Edition) Paul R. Halmos
Elementary Linear Algebra (Third Edition) Howard Anton
Single-Variable Calculus Robert A. Adams
The Elegant Universe Brian Greene
Modern College Algebra and Trigonometry Edwin F. Beckenbach, Irving Drooyan, Michael D. Grady
Dennery and Krzywicki
Mathematica A Systen for Doing Mathematics by Computer Stephen Wolfram
Tables of Functions Eugene Jahnke & Fritz Emde
Mathematical Expositions 10
Mathematics for Physicists
Implementing Derivatives Models by Les Clewlow and Chris Strickland
Statistical Analysis of Financial Data in R (2rd Edition)
Stochastic Calculus for Finance I
Stochastic Calculus for Finance II
Linux Kernel in a nutshell (a desktop quick reference)
Linux Kernel Development Robert Love
Roots by Alex Haley
The Old Man and The Sea
Marbles in my Underpants
The Ticking
Micrographica
Glen or Glenda (Film)



Data Structures & Algorithms:
Grokking Algorithms
Introduction to Algorithms
Algorithm Design Manual

Coding best practices:
Clean Code
Clean Architecture
Refactoring

Distributed Systems:
Understanding Distributed Systems
Designing Data Intensive Applications
Software Architecture: The Hard Parts

Dev Ops:
Learn DevOps

Machine Learning:
The 100-page Machine Learning Book
AI & ML for Coders
AI: A Modern Approach

Algorithms to live by (the computer science of human decisions)
Computational Physics Mark Newman
An Introduction to Statistical Mechanics and Thermodynamics Robert H. Swendsen
The Atlas of the Real World: Mapping the way we live
Quantum Mechanics in the Single Photon Laboratory
Psycho-Cybernetics, A New Way to Get More Living Out Life
Outliers: The Story of Success
Physics from Symmetry
Teach Yourself Physics
Physics from Finance
No-Nonsense Quantum Field Theory
No-Nonsense Classical Mechanics
No-Nonsense Electrodynamics
No-Nonsense Quantum Mechanics
Physics Travel Guide
Introduction to High Performance Scientific Computing SIAM
CUDA Programming Shane Cook
Numerical Methods in Computational Finance Daniel J. Duffy
Calculus for the Practical Man
Trigonometry for the Practical Man
Advanced Calculus by Woods
Methods of Theoretical Physics P.M. Morse and H. Feshback (3rd Ed.)
Beautiful Code by Andy Oram, Greg Wilson
The Staff Engineer's Path by Tanya Reilly
Strange Code by Ronald T. Kneusel
Seriously Good Software by Marco Faella
Good Code, Bad Code by Tom Long
Skills of a Successful Software Engineer by Fernando Doglio
Python Data Science Handbook, 2nd Edition by Jake VanderPlas
Storytelling with Data by Cole Nussbaurner Knaflic
A Programmer's Introduction to Mathematics by Jeremy Kun
Mage Merlins' unsolved mathematical mysteries
The Pathless Path
