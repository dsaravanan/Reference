# Debian 12 Bookworm GNU/Linux install

<Enter> activates button
<Space> select
<Tab>   move

0. Debian GNU/Linux BIOS Installer menu
    Install

1. Select a language: English

2. Select your location: India

3. Configure the keyboard: American English

4. Configure the network

    [Choose the primary network interface: wired network interface]

    Hostname: gusdt
    Domain name: [not required for home use]

5. Set up users and passwords
    root - the system administrative account Password: [root password]
    Re-enter password to verify: [root password]
    Full name for the new user: [First_name Last_name]
    Username for your account: [username]
    Choose a password for the new user: [user password]
    Re-enter password to verify: [user password]

6. Configure the clock: Select your time zone

7. Partition disks: Guided - use entire disk
    Select disk to partition: SCSI1 (0, 0, 0) (sda) - 500.1 GB
    Partitioning scheme: All files in one partition [single user]
                         Seperate /home partition [multi user]
                         Seperate /home, /var, and /tmp partitions [server]

    Overview of your currently configured partitions and mount points
    Finish partitioning and write changes to disk
    Write the changes to disks? <Yes>

    ---------- Installing the base system ----------

8. Configure the package manager:
    Debian archive mirror country: India
    Debian archive mirror: deb.debian.org
    HTTP proxy information: (blank for none)

    ---------- Configuring apt ----------

9. Configuring popularity-contest: https://popcon.debian.org/ <No>

10. Software selection:
        x   Standard system utilities

    ---------- Select and install software ----------

11. Finish the installation:
        Please choose <continue> to reboot.

    ---------- Installation is complete ----------

    [remove the installation media]


login as $user
su -
apt-get install doas
edit file /etc/doas.conf
doas apt-get update
doas apt-get upgrade
doas apt-get install x11-xserver-utils
doas apt-get install git
git clone https://git.suckless.org/dwm
git clone https://git.suckless.org/st
git clone https://git.suckless.org/slstatus
git clone https://git.suckless.org/dmenu
doas apt-get install make gcc libx11-dev libxft-dev libxinerama-dev xorg
doas apt-get install unclutter
doas apt-get install qutebrowser [run: adblock-update]
doas apt-get install scrot
doas apt-get install sxiv
doas apt-get install build-essential
doas apt-get install zathura
doas apt-get install texlive-full
doas apt-get install ffmpeg
doas apt-get install mpv
doas apt-get install vim
doas apt-get install alsa-utils
doas apt-get install w3m
doas apt-get install pandoc
doas apt-get install mpich
doas apt-get install inxi
doas apt-get install adb
doas apt-get install pdftk
doas apt-get install plocate
doas apt-get install hwinfo
doas apt-get install wordnet
doas apt-get remove wordnet-gui




Machine information:

Hewlett-Packard setup utility: BIOS key [F10]
Storage ----> Device configuration ----> Hard Disk [SATA0 500GB]
File ----> Save Changes and Exit ----> <Yes>

Advanced ----> Device Options:
                    USB EHCI Port Debug: [Disabled] ====> [Enabled]
                    USB3.0 BIOS Driver Support: [Disabled] ====> [Enabled]
