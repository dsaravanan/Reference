<h4 class="title"><a name="docker-mysql-getting-started"></a>2.5.6.1 Basic Steps for MySQL Server Deployment with Docker</h4>

</div>

</div>

</div>
<div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;">
<div class="admon-title">
Warning
</div>
<p>
          The MySQL Docker images maintained by the MySQL team are built
          specifically for Linux platforms. Other platforms are not
          supported, and users using these MySQL Docker images on them
          are doing so at their own risk. See
          <a class="link" href="deploy-mysql-nonlinux-docker.html" title="2.5.6.3 Deploying MySQL on Windows and Other Non-Linux Platforms with Docker">the discussion
          here</a> for some known limitations for running these
          containers on non-Linux operating systems.
</p>
</div>
<div class="itemizedlist">
<ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p><a class="xref" href="docker-mysql-getting-started.html#docker-download-image" title="Downloading a MySQL Server Docker Image">Downloading a MySQL Server Docker Image</a></p></li><li class="listitem"><p><a class="xref" href="docker-mysql-getting-started.html#docker-starting-mysql-server" title="Starting a MySQL Server Instance">Starting a MySQL Server Instance</a></p></li><li class="listitem"><p><a class="xref" href="docker-mysql-getting-started.html#docker-connecting-within-container" title="Connecting to MySQL Server from within the Container">Connecting to MySQL Server from within the Container</a></p></li><li class="listitem"><p><a class="xref" href="docker-mysql-getting-started.html#docker-shell-access" title="Container Shell Access">Container Shell Access</a></p></li><li class="listitem"><p><a class="xref" href="docker-mysql-getting-started.html#docker-stopping-deleting" title="Stopping and Deleting a MySQL Container">Stopping and Deleting a MySQL Container</a></p></li><li class="listitem"><p><a class="xref" href="docker-mysql-getting-started.html#docker-upgrading" title="Upgrading a MySQL Server Container">Upgrading a MySQL Server Container</a></p></li><li class="listitem"><p><a class="xref" href="docker-mysql-getting-started.html#docker-more-topics" title="More Topics on Deploying MySQL Server with Docker">More Topics on Deploying MySQL Server with Docker</a></p></li></ul>
</div>
<h5><a name="docker-download-image"></a>Downloading a MySQL Server Docker Image</h5>
<p>
        Downloading the server image in a separate step is not strictly
        necessary; however, performing this step before you create your
        Docker container ensures your local image is up to date. To
        download the MySQL Community Edition image, run this command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker pull mysql/mysql-server:<em class="replaceable">tag</em></code></pre><p>
        The <em class="replaceable"><code>tag</code></em> is the label for the image
        version you want to pull (for example, <code class="literal">5.5</code>,
        <code class="literal">5.6</code>, <code class="literal">5.7</code>,
        <code class="literal">8.0</code>, or <code class="literal">latest</code>). If
        <strong class="userinput"><code>:<em class="replaceable"><code>tag</code></em></code></strong> is
        omitted, the <code class="literal">latest</code> label is used, and the
        image for the latest GA version of MySQL Community Server is
        downloaded. Refer to the list of tags for available versions on
        the
        <a class="ulink" href="https://hub.docker.com/r/mysql/mysql-server/tags/" target="_top">mysql/mysql-server
        page in the Docker Hub</a>.
      </p><p>
        You can list downloaded Docker images with this command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal"><code class="language-terminal">shell&gt; docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
mysql/mysql-server   latest              3157d7f55f8d        4 weeks ago         241MB</code></pre><p>
        To download the MySQL Enterprise Edition image from the
        <a class="ulink" href="https://support.oracle.com/" target="_top">My Oracle
        Support</a> website, sign in to your Oracle account,
        download from <span class="guimenu">Patches and Updates</span> the
        <code class="filename">tar.zip</code> file for the Docker image
        (<code class="filename">mysql-commercial-<em class="replaceable"><code>version</code></em>_linux_x86_64_docker_tar.zip</code>),
        unzip it to obtain the tarball inside
        (<code class="filename">mysql-enterprise-server-<em class="replaceable"><code>version</code></em>.tar</code>),
        and then load the image by running this command:
</p><pre class="programlisting copytoclipboard line-numbers language-simple one-line"><code class="language-simple">docker load -i mysql-enterprise-server-<em class="replaceable">version</em>.tar</code></pre>
<h5><a name="docker-starting-mysql-server"></a>Starting a MySQL Server Instance</h5>
<p>
        To start a new Docker container for a MySQL Server, use the
        following command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker run --name=<em class="replaceable">container_name</em> -d <em class="replaceable">image_name</em>:<em class="replaceable">tag</em></code></pre><p>
        The image name can be obtained using the <span class="command"><strong>docker
        images</strong></span> command, as explained in
        <a class="xref" href="docker-mysql-getting-started.html#docker-download-image" title="Downloading a MySQL Server Docker Image">Downloading a MySQL Server Docker Image</a>. The
        <code class="option">--name</code> option, for supplying a custom name for
        your server container, is optional; if no container name is
        supplied, a random one is generated.
      </p><p>
        For example, to start a new Docker container for the MySQL
        Community Server, use this command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker run --name=mysql1 -d mysql/mysql-server:8.0</code></pre><p>
        To start a new Docker container for the MySQL Enterprise Server
        with a Docker image downloaded from My Oracle Support, use this
        command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker run --name=mysql1 -d mysql/enterprise-server:8.0</code></pre><p>
        If the Docker image of the specified name and tag has not been
        downloaded by an earlier <span class="command"><strong>docker pull</strong></span> or
        <span class="command"><strong>docker run</strong></span> command, the image is now
        downloaded. Initialization for the container begins, and the
        container appears in the list of running containers when you run
        the <span class="command"><strong>docker ps</strong></span> command. For example:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal"><code class="language-terminal">shell&gt; docker ps
CONTAINER ID   IMAGE                COMMAND                  CREATED             STATUS                              PORTS                NAMES
a24888f0d6f4   mysql/mysql-server   "/entrypoint.sh my..."   14 seconds ago      Up 13 seconds (health: starting)    3306/tcp, 33060/tcp  mysql1</code></pre><p>
        The container initialization might take some time. When the
        server is ready for use, the <code class="literal">STATUS</code> of the
        container in the output of the <span class="command"><strong>docker ps</strong></span>
        command changes from <code class="literal">(health: starting)</code> to
        <code class="literal">(healthy)</code>.
      </p><p>
        The <code class="option">-d</code> option used in the <span class="command"><strong>docker
        run</strong></span> command above makes the container run in the
        background. Use this command to monitor the output from the
        container:

</p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker logs mysql1</code></pre><p>
      </p><p>
        Once initialization is finished, the command's output is going
        to contain the random password generated for the root user;
        check the password with, for example, this command:

</p><pre class="programlisting copytoclipboard line-numbers language-terminal"><code class="language-terminal">shell&gt; docker logs mysql1 2&gt;&amp;1 | grep GENERATED
GENERATED ROOT PASSWORD: Axegh3kAJyDLaRuBemecis&amp;EShOs</code></pre><p>
</p>
<h5><a name="docker-connecting-within-container"></a>Connecting to MySQL Server from within the Container</h5>
<p>
        Once the server is ready, you can run the
        <a class="link" href="mysql.html" title="4.5.1 mysql — The MySQL Command-Line Client"><span class="command"><strong>mysql</strong></span></a> client within the MySQL Server
        container you just started, and connect it to the MySQL Server.
        Use the <span class="command"><strong>docker exec -it</strong></span> command to start a
        <a class="link" href="mysql.html" title="4.5.1 mysql — The MySQL Command-Line Client"><span class="command"><strong>mysql</strong></span></a> client inside the Docker container you
        have started, like the following:

</p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker exec -it mysql1 mysql -uroot -p</code></pre><p>

        When asked, enter the generated root password (see the last step
        in <a class="xref" href="docker-mysql-getting-started.html#docker-starting-mysql-server" title="Starting a MySQL Server Instance">Starting a MySQL Server Instance</a> above on how
        to find the password). Because the
        <a class="link" href="docker-mysql-more-topics.html#docker_var_mysql_onetime_password"><code class="option">MYSQL_ONETIME_PASSWORD</code></a>
        option is true by default, after you have connected a
        <a class="link" href="mysql.html" title="4.5.1 mysql — The MySQL Command-Line Client"><span class="command"><strong>mysql</strong></span></a> client to the server, you must reset
        the server root password by issuing this statement:

</p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">mysql&gt; ALTER USER 'root'@'localhost' IDENTIFIED BY '<em class="replaceable">password</em>';</code></pre><p>

        Substitute <em class="replaceable"><code>password</code></em> with the password
        of your choice. Once the password is reset, the server is ready
        for use.
</p>
<h5><a name="docker-shell-access"></a>Container Shell Access</h5>
<p>
        To have shell access to your MySQL Server container, use the
        <span class="command"><strong>docker exec -it</strong></span> command to start a bash shell
        inside the container:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal"><code class="language-terminal">shell&gt; docker exec -it mysql1 bash 
bash-4.2#</code></pre><p>
        You can then run Linux commands inside the container. For
        example, to view contents in the server's data directory inside
        the container, use this command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal"><code class="language-terminal">bash-4.2# ls /var/lib/mysql
auto.cnf    ca.pem	     client-key.pem  ib_logfile0  ibdata1  mysql       mysql.sock.lock	   private_key.pem  server-cert.pem  sys
ca-key.pem  client-cert.pem  ib_buffer_pool  ib_logfile1  ibtmp1   mysql.sock  performance_schema  public_key.pem   server-key.pem</code></pre>
<h5><a name="docker-stopping-deleting"></a>Stopping and Deleting a MySQL Container</h5>
<p>
        To stop the MySQL Server container we have created, use this
        command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker stop mysql1</code></pre><p>
        <span class="command"><strong>docker stop</strong></span> sends a SIGTERM signal to the
        <a class="link" href="mysqld.html" title="4.3.1 mysqld — The MySQL Server"><span class="command"><strong>mysqld</strong></span></a> process, so that the server is shut
        down gracefully.
      </p><p>
        Also notice that when the main process of a container
        (<a class="link" href="mysqld.html" title="4.3.1 mysqld — The MySQL Server"><span class="command"><strong>mysqld</strong></span></a> in the case of a MySQL Server
        container) is stopped, the Docker container stops automatically.
      </p><p>
        To start the MySQL Server container again:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker start mysql1</code></pre><p>
        To stop and start again the MySQL Server container with a single
        command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker restart mysql1</code></pre><p>
        To delete the MySQL container, stop it first, and then use the
        <span class="command"><strong>docker rm</strong></span> command:
      </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker stop mysql1</code></pre><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker rm mysql1</code></pre><p>
        If you want the
        <a class="link" href="docker-mysql-more-topics.html#docker-persisting-data-configuration" title="Persisting Data and Configuration Changes">Docker
        volume for the server's data directory</a> to be deleted at
        the same time, add the <code class="literal">-v</code> option to the
        <span class="command"><strong>docker rm</strong></span> command.
</p>
<h5><a name="docker-upgrading"></a>Upgrading a MySQL Server Container</h5>
<div class="important" style="margin-left: 0.5in; margin-right: 0.5in;">
<div class="admon-title">
Important
</div>
<div class="itemizedlist">
<ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
              Before performing any upgrade to MySQL, follow carefully
              the instructions in <a class="xref" href="upgrading.html" title="2.11 Upgrading MySQL">Section 2.11, “Upgrading MySQL”</a>. Among
              other instructions discussed there, it is especially
              important to back up your database before the upgrade.
            </p></li><li class="listitem"><p>
              The instructions in this section require that the server's
              data and configuration have been persisted on the host.
              See <a class="xref" href="docker-mysql-more-topics.html#docker-persisting-data-configuration" title="Persisting Data and Configuration Changes">Persisting Data and Configuration Changes</a>
              for details.
</p></li></ul>
</div>

</div>
<p>
        Follow these steps to upgrade a Docker installation of MySQL 5.7
        to 8.0:

        
</p>
<div class="itemizedlist">
<ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
            Stop the MySQL 5.7 server (container name is
            <code class="literal">mysql57</code> in this example):
          </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker stop mysql57</code></pre></li><li class="listitem"><p>
            Download the MySQL 8.0 Server Docker image. See instructions
            in <a class="xref" href="docker-mysql-getting-started.html#docker-download-image" title="Downloading a MySQL Server Docker Image">Downloading a MySQL Server Docker Image</a>; make sure you
            use the right tag for MySQL 8.0.
          </p></li><li class="listitem"><p>
            Start a new MySQL 8.0 Docker container (named
            <code class="literal">mysql80</code> in this example) with the old
            server data and configuration (with proper modifications if
            needed—see <a class="xref" href="upgrading.html" title="2.11 Upgrading MySQL">Section 2.11, “Upgrading MySQL”</a>) that have been
            persisted on the host (by
            <a class="ulink" href="https://docs.docker.com/engine/reference/commandline/service_create/#add-bind-mounts-or-volumes" target="_top">bind-mounting</a>
            in this example). For the MySQL Community Server, run this
            command:
          </p><pre class="programlisting copytoclipboard line-numbers language-terminal"><code class="language-terminal">docker run --name=mysql80 \
   --mount type=bind,src=/path-on-host-machine/my.cnf,dst=/etc/my.cnf \
   --mount type=bind,src=/path-on-host-machine/datadir,dst=/var/lib/mysql \        
   -d mysql/mysql-server:8.0</code></pre><p>
            If needed, adjust <code class="literal">mysql/mysql-server</code> to
            the correct repository name—for example, replace it
            with


            <code class="literal">mysql/enterprise-server</code> for MySQL Enterprise Edition images
            downloaded from <a class="ulink" href="https://support.oracle.com/" target="_top">My
            Oracle Support</a>.
          </p></li><li class="listitem"><p>
            Wait for the server to finish startup. You can check the
            status of the server using the <span class="command"><strong>docker ps</strong></span>
            command (see <a class="xref" href="docker-mysql-getting-started.html#docker-starting-mysql-server" title="Starting a MySQL Server Instance">Starting a MySQL Server Instance</a>
            for how to do that).
          </p></li><li class="listitem"><p>
            <span class="emphasis"><em>For MySQL 8.0.15 and earlier:</em></span> Run the
            <a class="link" href="mysql-upgrade.html" title="4.4.5 mysql_upgrade — Check and Upgrade MySQL Tables">mysql_upgrade</a> utility
            in the MySQL 8.0 Server container (not required for MySQL
            8.0.16 and later):
          </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker exec -it mysql80 mysql_upgrade -uroot -p</code></pre><p>
            When prompted, enter the root password for your old MySQL
            5.7 Server.
          </p></li><li class="listitem"><p>
            Finish the upgrade by restarting the MySQL 8.0 Server
            container:
          </p><pre class="programlisting copytoclipboard line-numbers language-terminal one-line"><code class="language-terminal">docker restart mysql80</code></pre></li></ul>
</div>
<h5><a name="docker-more-topics"></a>More Topics on Deploying MySQL Server with Docker</h5>
<p>
        For more topics on deploying MySQL Server with Docker like
        server configuration, persisting data and configuration, server
        error log, and container environment variables, see
        <a class="xref" href="docker-mysql-more-topics.html" title="2.5.6.2 More Topics on Deploying MySQL Server with Docker">Section 2.5.6.2, “More Topics on Deploying MySQL Server with Docker”</a>.
</p>
