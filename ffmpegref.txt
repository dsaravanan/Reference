# convert *.mp3 to *.wav
ffmpeg -i input.mp3 output.wav

# convert *.mp4 to *.mp3
ffmpeg -i input.mp4 output.mp3

# convert *.mkv to *.mp4
ffmpeg -i input.mkv -codec copy output.mp4

# ffmpeg
ffmpeg -i filename.mp3/filename.mp4 # to read entire history

# remove logo with the ffmpeg delogo
ffplay input.mp4 -vf "delogo=x=1400:y=20:w=500:h=480:show=1"

# combine/concat multiple audio tracks into single audio
ffmpeg -f concat -safe 0 -1 list.txt audio.mp3

list.txt contains paths to all segments:
file audiofile1.mp3
file audiofile2.mp3
file audiofile3.mp3

# combine one image + one audio file to make one video using ffmpeg
ffmpeg -r 1 -loop 1 -i image.jpg -i audio.mp3 -acodec copy -r 1 -shortest -vf
scale=1280:720 video.mp4

# combine/concat multiple video files into single video
ffmpeg -f concat -i list.txt video.mp4

list.txt contains paths to all segments:
file videofile1.mp4
file videofile2.mp4
file videofile3.mp4

# trim files from the beginning to a specific duration
ffmpeg -i input_audio.wav -t 5 output_audio.wav
ffmpeg -i input_video.mp4 -t 00:00:05 output_video.mp4

This works for both video and audio files. Both of the commands above do the
same thing: save the first 5 seconds of the input file to the output file. I
have used to different ways of inputting the duration: a single number (number
of seconds) and HH:MM:SS (hours, minutes, seconds).

You can go even further by specifying a start time with -ss, and even an end
time with -to:

ffmpeg -i input_audio.mp3 -ss 00:01:14 output_audio.mp3
ffmpeg -i input_audio.wav -ss 00:00:30 -t 10 output_audio.wav
ffmpeg -i input_video.h264 -ss 00:01:30 -to 00:01:40 output_video.h264
ffmpeg -i input_audio.ogg -ss 5 output_audio.ogg

You can see start time (-ss HH:MM:SS), duration (-t duration) in seconds, end
time (-to HH:MM:SS), and start time (-s duration) in seconds (starting after
duration seconds).

# trim videos with below example time-line
# 00:00:00 - 00:01:55.030  ----  00:03:02 - 00:21:09
ffmpeg -i singapore.mp4 -ss 00:00:00 -to 00:01:55.030 output1.mp4
ffmpeg -i singapore.mp4 -ss 00:03:02 -to 00:21:09 output2.mp4
