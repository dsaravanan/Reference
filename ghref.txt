gh reference

# install gh [https://github.com/cli/cli/releases]
wget https://github.com/cli/cli/releases/download/v2.60.1/gh_2.60.1_linux_amd64.tar.gz
tar -xzvf gh_2.60.1_linux_amd64.tar.gz
cd gh_2.60.1_linux_amd64/bin/
doas chown root:root gh
doas mv gh /usr/local/bin/
gh --version

# gh man pages
manpath
/usr/local/man:/usr/local/share/man:/usr/share/man

cd gh_2.60.1_linux_amd64/share/
mv man /home/user/.local/share/

.bashrc
export PATH=$PATH:$HOME/.local/share/man

source .bashrc

manpath
/usr/local/man:/usr/local/share/man:/usr/share/man:/home/saran/.local/share/man

# create personal access token
You can create as many personal access tokens as you like:
1. In the upper-right corner, click your profile avatar, then click Settings.
2. In the left sidebar, click Developer settings.
3. In the left sidebar, under Personal access tokens, click Tokens (classic).
4. Select Generate new token, then click Generate new token (classic).
5. In the "Note" field, give your token a descriptive name.
7. To give your token an expiration, select Expiration, then choose a default option
   or click Custom to enter a date.
8. Select the scopes you'd like to grant this token. To use your token to access
   repositories from the command line, select repo. A token with no assigned scopes
   can only access public information.
9. Click Generate token.

Reference:
https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/
managing-your-personal-access-tokens

# start interactive setup
gh auth login

# authenticate against github.com by reading the token from a file
gh auth login --with-token < token.txt

# authenticate with specific host
gh auth login --hostname enterprise.internal

# clone repository
gh repo clone <repo_name> # clone repository from your own account
gh repo clone jeffwass/Physics_Simulations # clone repository from specific account

# clone repository, overriding git protocol configuration
gh repo clone https://github.com/jeffwass/Physics_Simulations.git
gh repo clone git@github.com:jeffwass/Physics_Simulations.git

# clone repository to a custom directory
gh repo clone jeffwass/Physics_Simulations workspace/Physics_Simulations

# create repository interactively
gh repo create

# create new remote repository and clone it locally
gh repo create <repo_name> --public --clone

# create new remote repository in a different organization
gh repo create jeffwass/Chemistry_Simulations --public

# create remote repository from the current directory
gh repo create <repo_name> --private --source=. --remote=upstream

# edit repository description
gh repo edit namespace/repo -d "repository description"

# create issues
gh issue create -t "issue title" -b "issue body content" -a "assignee"

# create pull requests
gh pr create -t "pull request title" -b "pull request body content" -a "assignee"

# view action workflows
gh run list
gh workflow run <workflow filename>

# open your current repo in the browser
gh browse
gh browse -b "branch name"

# create and update project boards
gh auth refresh -s project,read:project
gh project create --owner "owner" --title "title"

# create edit, and view releases
gh release create --title "title" --notes "notes"

# start and stop codespaces
gh codespace create -b "branch"

# display the description and the README of a GitHub repository
gh repo view dsarvan/pyqtcalc
gh repo view dsarvan/pyqtcalc --branch main # view specific branch of the repository
gh repo view dsarvan/pyqtcalc --web # open the repository in a web browser instead
gh repo view # will display the repository you're currently in

# list common repository licenses
gh repo license list
For even more licenses, visit https://choosealicense.com/appendix

# view a specific repository license by license key or SPDX ID
gh repo license view MIT # view the MIT license from SPDX ID
gh repo license view mit # view the MIT license from license key
gh repo license view AGPL-3.0 # view the GNU AGPL-3.0 license from SPDX ID
gh repo license view agpl-3.0 # view the GNU AGPL-3.0 license from license key
gh repo license view MIT > LICENSE # create a LICENSE with the MIT license

Reference:
https://cli.github.com/manual/
https://dev.to/github/top-10-tips-for-using-github-from-the-command-line-1me6
