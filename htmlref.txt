# W3C Markup Validation
https://validator.w3.org/

# Dealing with files
https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web
/Dealing_with_files

# Tables generator
https://www.tablesgenerator.com/html_tables

# Semantic HTML Tags
<abbr>			Abbreviation
<acronym>		Acronym
<blockquote>	Long quotation
<dfn>			Definition
<address>		Address for author(s) of the document
<cite>			Citation
<code>			Code reference
<tt>			Teletype text
<div>			Logical division
<span>			Generic inline style container
<del>			Deleted text
<ins>			Inserted text
<em>			Emphasis
<strong>		Strong emphasis
<h1>			First-level headline
<h2>			Second-level headline
<h3>			Third-level headline
<h4>			Fourth-level headline
<h5>			Fifth-level headline
<h6>			Sixth-level headline
<hr>			Thematic break
<kbd>			Text to be entered by the user
<pre>			Pre-formatted text
<q>				Short inline quotation
<samp>			Sample output
<sub>			Subscript
<sup>			Superscript
<var>			Variable or user defined text

<b> </b>         bold
<i> </i>         italic
<u> </u>         underline
<font> </font>   font

# convert LaTeX to HTML
htlatex document.tex
lwarp - converts LaTeX to HTML [https://ctan.org/pkg/lwarp]

# automatic dimensions
<img src="pendulum.svg" style="width: 650pt; height: auto;" />

# to maximize the image
remove "width and height" in *.svg file

# search and replace content
replace '2020 Dayalan Saravanan' with '2024 Dayalan Saravanan':
sed -i 's/2020 Dayalan Saravanan/2024 Dayalan Saravanan/g' *.html

replace '&nbsp;nbsp;' with nothing:
sed -i 's/&nbsp;&nbsp;//g' *.html

replace '<h2>Dayalan Saravanan</h2>' with <h3>Dayalan Saravanan</h3>':
sed -i 's#<h2>Dayalan Saravanan</h2>#<h3>Dayalan Saravanan</h3>#' *.html

replace 'type="text/css" href="style.css"' with 'href="style.css"':
sed -i 's:type=\"text\/css\" href=\"style.css\"\/:href=\"style.css\":' *.html

# copy lines 9,10,11 from index.html and paste after line 8 in xmonad.html
sed -i 8r<(sed '9,11!d' index.html) xmonad.html

# spell check
ispell -H file.html

# correct and tidy up the markup of HTML, XHTML, and XML files
tidy file.html

# fonts
Arial is the most widely used sans-serif font on the web. It was created for
printers who wanted to use the popular Helvetica font without the licensing
fees. Therefore, they're virtually identical.
Arial and members of the Arial font family are considered the safest web fonts
because they're available on all major operating systems.

Reference:
CSE 154 Unofficial Style Guide - https://courses.cs.washington.edu/courses/cse154/17au/styleguide/index.html
Formatting & Indenting HTML - https://granneman.com/webdev/coding/formatting-and-indenting-your-html
Programmatically producing HTML from the command-line - http://www.compciv.org/topics/web/heredocs_and_html/
Document command-line syntax - https://developers.google.com/style/code-syntax
Indenting with HTML - http://www.blooberry.com/indexdot/html/topics/indent.htm
HTML formatting - https://developers.google.com/style/html-formatting
Prettier CLI - https://prettier.io/docs/en/cli.html
Cleanup your HTML - https://vim.fandom.com/wiki/Cleanup_your_HTML
Use Noto fonts - https://fonts.google.com/noto/use#how-are-noto-fonts-organizedo
Google Fonts - https://fonts.google.com/
CSS-TRICKS - https://css-tricks.com/



Tidy Documentation - https://www.html-tidy.org/documentation


HaWo's Linux Introduction - http://www.macs.hw.ac.uk/~hwloidl/Courses/LinuxIntro/t1.html
A Practical Guide to Linux Commands, Editors, and Shell Programming -
http://www.sobell.com/CMDREF1/
