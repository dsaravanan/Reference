<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>How To Install Node.js on Debian 10 | DigitalOcean</title>

      <h1 class="content-title Tutorial-header">How To Install Node.js on Debian 10</h1>

        <p class="names">By <a href="/community/users/bpb">Brennen Bearnes</a> and  <a href="/community/users/katjuell">Kathleen Juell</a></p>

      <span class="meta-section date-views">
  
  <span class="meta-section timestamp"><span class="tutorial-date-text">Published on</span><span class="tutorial-date">August 28, 2019</span></span>
</span>

<div class="content-body tutorial-content" data-growable-markdown>
  
  <h3 id="introduction">Introduction</h3>

<p><a href="https://nodejs.org/en/">Node.js</a> is a JavaScript platform for general-purpose programming that allows users to build asynchronous network applications quickly. By leveraging JavaScript on both the front and backend, Node.js can make web application development more consistent and integrated.</p>

<p>In this guide, we&rsquo;ll show you how to get started with Node.js on a Debian 10 server. We will discuss installing Node from the default Debian repository, using a more up-to-date PPA repository, and using NVM (Node Version Manager) to install and activate different versions of Node.</p>

<p>Finally, we will show how to uninstall these different versions of Node.</p>

<h2 id="prerequisites">Prerequisites</h2>

<p>This guide assumes that you are using Debian 10. Before you begin, you should have a non-root user with sudo privileges set up on your system. You can learn how to set this up by following the <a href="https://www.digitalocean.com/community/tutorials/initial-server-setup-with-debian-10">initial server setup for Debian 10</a> tutorial.</p>

<h2 id="installing-the-official-debian-node-js-package">Installing the Official Debian Node.js Package</h2>

<p>Debian contains a version of Node.js in its default repositories. At the time of writing, this version is 10.15.2, which will reach end-of-life on April 1, 2021. At this date it will no longer be supported with security and bug fixes. If you would like to experiment with Node using an easy-to-install, stable, and long-term option, then installing from the Debian repo may make sense. </p>

<p>To get Node.js from the default Debian software repository, you can use the <code>apt</code> package manager. First, refresh your local package index: </p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">sudo apt update
</li></ul></code></pre>
<p>Then install the Node.js package, and <code>npm</code> the Node Package Manager:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">sudo apt install nodejs npm
</li></ul></code></pre>
<p>To verify that the install was successful, run the <code>node</code> command with the <code>-v</code> flag to get the version:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">node -v
</li></ul></code></pre><pre class="code-pre "><code><div class="secondary-code-label " title="Output">Output</div>v<span class="highlight">10.15.2</span>
</code></pre>
<p>If you need a more recent version of Node.js than this, the next two sections will explain other installation options.</p>

<h2 id="installing-using-a-ppa">Installing Using a PPA</h2>

<p>To work with a more recent version of Node.js, you can install from a <em>PPA</em> (personal package archive) maintained by <a href="https://nodesource.com">NodeSource</a>. This is an alternate repository that still works with `apt, and will have more up-to-date versions of Node.js than the official Debian repositories. NodeSource has PPAs available for Node versions from 0.10 through to 12.</p>

<p>Let&rsquo;s install the PPA now. This will add the repository to our package list and allow us to install the new packages using <code>apt</code>.</p>

<p>From your home directory, use <code>curl</code> to retrieve the installation script for your preferred Node.js version, making sure to replace <code><span class="highlight">12.x</span></code> with your preferred version string (if different):</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">cd ~
</li><li class="line" data-prefix="$">curl -sL https://deb.nodesource.com/setup_<span class="highlight">12.x</span> -o nodesource_setup.sh
</li></ul></code></pre>
<p>You can inspect the contents of this script with <code>nano</code> or your preferred text editor:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nano nodesource_setup.sh
</li></ul></code></pre>
<p>If everything looks OK, exit your text editor and run the script using <code>sudo</code>:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">sudo bash nodesource_setup.sh
</li></ul></code></pre>
<p>The PPA will be added to your configuration and your local package cache will be updated automatically. Now you can install the <code>nodejs</code> package in the same way you did in the previous step:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">sudo apt install nodejs
</li></ul></code></pre>
<p>We don&rsquo;t need to install a separate package for <code>npm</code> in this case, as it is included in the <code>nodejs</code> packae.</p>

<p>Verify the installation by running <code>node</code> with the <code>-v</code> version option:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">node -v
</li></ul></code></pre><pre class="code-pre "><code><div class="secondary-code-label " title="Output">Output</div>v<span class="highlight">12.8.0</span>
</code></pre>
<p><code>npm</code> uses a configuration file in your home directory to keep track of updates. It will be created the first time you run <code>npm</code>. Execute this command to verify that <code>npm</code> is installed and to create the configuration file:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">npm -v
</li></ul></code></pre><pre class="code-pre "><code><div class="secondary-code-label " title="Output">Output</div><span class="highlight">6.10.2</span>
</code></pre>
<p>In order for some <code>npm</code> packages to work (those that require compiling code from source, for example), you will need to install the <code>build-essential</code> package:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">sudo apt install build-essential
</li></ul></code></pre>
<p>You now have the necessary tools to work with <code>npm</code> packages that require compiling code from source.</p>

<h2 id="installing-using-nvm">Installing Using NVM</h2>

<p>An alternative to installing Node.js through <code>apt</code> is to use a tool called <code>nvm</code>, which stands for &ldquo;Node Version Manager&rdquo;. Rather than working at the operating system level, <code>nvm</code> works at the level of an independent directory within your user&rsquo;s home directory. This means that you can install multiple self-contained versions of Node.js without affecting the entire system. </p>

<p>Controlling your environment with <code>nvm</code> allows you to access the newest versions of Node.js while also retaining and managing previous releases. It is a different utility from <code>apt</code>, however, and the versions of Node.js that you manage with it are distinct from those you manage with <code>apt</code>. </p>

<p>To download the <code>nvm</code> installation script from the <a href="https://github.com/creationix/nvm">project&rsquo;s GitHub page</a>, you can use <code>curl</code>. Note that the version number may differ from what is highlighted here: </p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">curl -sL https://raw.githubusercontent.com/nvm-sh/nvm/v<span class="highlight">0.34.0</span>/install.sh -o install_nvm.sh
</li></ul></code></pre>
<p>Inspect the installation script with <code>nano</code>:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nano install_nvm.sh
</li></ul></code></pre>
<p>If the script looks OK, exit your text editor and run the script with <code>bash</code>:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">bash install_nvm.sh
</li></ul></code></pre>
<p>We don&rsquo;t need <code>sudo</code> here because <code>nvm</code> is not installed into any privileged system directories. It will instead install the software into a subdirectory of your home directory at <code>~/.nvm</code>. It will also add some configuration to your <code>~/.profile</code> file to enable the new software.</p>

<p>To gain access to the <code>nvm</code> functionality, you&rsquo;ll need to either log out and log back in again or source the <code>~/.profile</code> file so that your current session knows about the changes:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">source ~/.profile
</li></ul></code></pre>
<p>With <code>nvm</code> installed, you can install isolated Node.js versions. For information about the versions of Node.js that are available, type:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm ls-remote
</li></ul></code></pre><pre class="code-pre "><code><div class="secondary-code-label " title="Output">Output</div>. . .
       v10.16.2   (Latest LTS: Dubnium)
        v11.0.0
        v11.1.0
        v11.2.0
        v11.3.0
        v11.4.0
        v11.5.0
        v11.6.0
        v11.7.0
        v11.8.0
        v11.9.0
       v11.10.0
       v11.10.1
       v11.11.0
       v11.12.0
       v11.13.0
       v11.14.0
       v11.15.0
        v12.0.0
        v12.1.0
        v12.2.0
        v12.3.0
        v12.3.1
        v12.4.0
        v12.5.0
        v12.6.0
        v12.7.0
        v12.8.0
</code></pre>
<p>As you can see, the current LTS version at the time of this writing is v10.16.2. You can install that by typing:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm install <span class="highlight">10.16.2</span>
</li></ul></code></pre>
<p>Usually, <code>nvm</code> will switch to use the most recently installed version.  You can tell <code>nvm</code> to use the version you just downloaded by typing:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm use <span class="highlight">10.16.2</span>
</li></ul></code></pre>
<p>As always, you can verify the Node.js version currently being used by typing:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">node -v
</li></ul></code></pre><pre class="code-pre "><code><div class="secondary-code-label " title="Output">Output</div>v<span class="highlight">10.16.2</span>
</code></pre>
<p>If you have multiple Node.js versions, you can see what is installed by typing:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm ls
</li></ul></code></pre>
<p>If you wish to default to one of the versions, type:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm alias default <span class="highlight">10.16.2</span>
</li></ul></code></pre>
<p>This version will be automatically selected when a new session spawns.  You can also reference it by the alias like this:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm use default
</li></ul></code></pre>
<p>Each version of Node.js will keep track of its own packages and has <code>npm</code> available to manage these.</p>

<h2 id="removing-node-js">Removing Node.js</h2>

<p>You can uninstall Node.js using <code>apt</code> or <code>nvm</code>, depending on the version you want to target. To remove versions installed from the Debian repository or from the PPA, you will need to work with the <code>apt</code> utility at the system level.</p>

<p>To remove either of these versions, type the following:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">sudo apt remove nodejs
</li></ul></code></pre>
<p>This command will remove the package and the configuration files. </p>

<p>To uninstall a version of Node.js that you have enabled using <code>nvm</code>, first determine whether or not the version you would like to remove is the current active version:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm current
</li></ul></code></pre>
<p>If the version you are targeting is <strong>not</strong> the current active version, you can run:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm uninstall <span class="highlight">node_version</span>
</li></ul></code></pre>
<p>This command will uninstall the selected version of Node.js.</p>

<p>If the version you would like to remove <strong>is</strong> the current active version, you must first deactivate <code>nvm</code> to enable your changes:</p>
<pre class="code-pre command prefixed"><code class="code-highlight language-bash"><ul class="prefixed"><li class="line" data-prefix="$">nvm deactivate
</li></ul></code></pre>
<p>You can now uninstall the current version using the <code>uninstall</code> command above, which will remove all files associated with the targeted version of Node.js except the cached files that can be used for reinstallation. </p>

<h2 id="conclusion">Conclusion</h2>

<p>There are a quite a few ways to get up and running with Node.js on your Debian 10 server. Your circumstances will dictate which of the above methods is best for your needs. While using the packaged version in the Debian repository is an option for experimentation, installing from a PPA and working with <code>npm</code> or <code>nvm</code> offers additional flexibility.</p>

</div>

  </body>
</html>
