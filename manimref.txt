Installation on GNU Linux:

Setp 1: Install LaTeX (TeX live)
$ sudo apt-get install texlive-full

Step 2: Install Python 3.7 
$ sudo apt-get install python3.7-minimal

Step 3: Install PIP (need curl)
$ sudo apt-get install curl
$ mkdir InstallManim
$ cd InstallManim
$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
$ sudo python3.7 get-pip.py

Step 4: Install FFmpeg, SoX and Cairo dependencies
$ sudo apt-get install ffmpeg
$ sudo apt-get install sox
$ sudo apt-get install libcairo2-dev libjpeg-dev libgif-dev python3-dev libffi-dev
$ python3.7 -m pip install pyreadline
$ python3.7 -m pip install pydub

Step 5: Download manim and move it to a directory that must not have spaces in the name

Step 6: Open this directory with a terminal and run the following command to install
Python plog-ins
$ python3.7 -m pip install -r requirements.txt


Example #1:
from manim import *

class squarecircle(Scene):
    def construct(self):
        circle = Circle(color=BLUE, fill_opacity=0.5)
        square = Square(color=GREEN, fill_opacity=0.8)
        square.next_to(circle, RIGHT)
        self.add(circle, square)

Scene: animation canvas
Circle, Square: manim objects
color, fill_opacity: properties of manim objects
colors (BLUE, GREEN) and directions (UP, DOWN, RIGHT, ..): some global constants

generating output:
* write code in program.py
* then run in terminal (qm = quality medium, p = preview)
  $ manim -qm -p program.py squarecircle
