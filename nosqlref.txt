docker run -it -v /home/raman/.mongodata:/data/db -p 27017:27017 --name mongodb -d mongo
docker logs mongodb
docker start mongodb
docker exec -it mongodb bash
mongo -host localhost -port 27017
show startupWarnings
exit

apt-get install vim openssh-server openssh-client
ssh-keygen -t rsa
sudo service ssh status
sudo service ssh start
ssh user@<ipaddress>

# current version of mongodb
version()

# list of commands
db.help()

# database name, number of collection and documents in the database
db.stats()

Note: In MongoDB, the default database is test.

# database list
show databases/dbs
[to display database, insert atleast one document into it]

# create new database if doesn't exist else return existing database 
use <database>

# check currently selected database
db

# collection list
show collections

# documents list
db.<collection>.find()
db.<collection>.find().pretty()

# insert
db.<collection>.insert({key: 'value'})

# drop database
db.dropDatabase()
[delete selected database]
[if not selected any database, then it delete default 'test' database]

# create collection
db.createCollection(name, options)
[name - name of the collection, options - a document which can be used to specify configurations for the collection]

db.createCollection("Departments", {capped: true, size: 5242880, max: 5000})
[this will create a collection named "Departments", with maximum size of 5 MB and maximum of 5000 documents]

# collection list
show collections

# drop collection
db.<collection>.drop()
[drop() method will return true, if the selected collection is dropped successfully, otherwise it will return false]

# CRUD operations

structure of MongoDB document
{
	field1: value1;
	field2: value2;
	.
	.	
	.
	fieldN: valueN;
}

1. INSERT documents
db.<collection>.insert(document)
[_id is 12 bytes hexadecimal number unique for every document in a collection]
[_id: ObjectId (4 bytes timestamp, 3 bytes machineId, 2 bytes processId, 3 bytes incrementer)]

db.<collection>.insertOne()    # to insert one document
db.<collection>.insertMany([]) # to insert multiple documents

post = {title: "My blog post", content: "Here's is my blog post.", date: new Date()}
db.<collection>.insert(post)

2. QUERY(Find)
db.<collection>.find() # query all documents in a collection

criteria = {name: "Raman"}
db.<collection>.find(criteria) # query documents of a collection based on a criteria

db.<collection>.find().pretty() # it display the results in a formatted way
db.<collection>.findOne() # it returns only one document

Projection:
db.<collection>.find(query_document, projection_document)
projection_doc = {"_id": 0, "Date": 1, "Time": 1, "Title": 1, "Speaker": 1}
db.<collection>.find({}, projection_doc)

3. UPDATE
db.<collection>.update(criteria, update)

db.<collection>.update({name: 'MongoDB overview'}, {$set: {name: 'New MongoDB overview'}})
db.<collection>.update({name: 'MongoDB overview'}, {$set: {name: 'New MongoDB overview'}}, {multi: true})

4. DELETE document
db.<collection>.remove(deletion_criteria)

Remove all documents: db.<collection>.remove({})

# count No. of documents 
db.<collection>.countDocuments({}) # count all documents
db.<collection>.countDocuments({Salary: 71000}) # count all documents with field Salary with value 71000

# limit No. of documents display
db.<collection>.find().limit(N) # display 1st N documents

# skip No. of documents display
db.<collection>.find().skip(N) # skip N documents and display

# skip with limit documents display
db.<collection>.find().skip(1).limit(2)
[1 document is skipped and the display is limited to 2 documents]

# sort documents
db.<collection>.find().sort({Field: sorting_order})
[sorting order 1 for ascending and -1 for descending]

db.<collection>.find().sort({"Salary": 1})
db.<collection>.find().sort({"Salary": -1})

# comparison operators
db.<collection>.find({Salary: {$eq: 71000}})			# equals
db.<collection>.find({Salary: {$ne: 71000}})   			# not equals
db.<collection>.find({Salary: {$lt: 75000}})            # less than
db.<collection>.find({Salary: {$lte: 75000}})           # less than equals
db.<collection>.find({Salary: {$gt: 75000}})            # greater than
db.<collection>.find({Salary: {$gte: 75000}})           # greater than equals
db.<collection>.find({Salary: {$in: [62500, 71000]}})   # values in an array
db.<collection>.find({Salary: {$nin: [62500, 71000]}})  # values not in an array






























# Notes
For multi-machine replication:
1. Benefit: Data is always highly available
2. If master fails any of the secondary will act as master (on the basis of election)

robo 3t 1.4

bindIP: localhost, hostname, ipaddress

ps -ef | grep mongo*
sudo kill -9 3455

fork = create process
