CUDA for Python on Ubuntu 20.04:

Install CUDA on Ubuntu 20.04:
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget https://developer.download.nvidia.com/compute/cuda/11.4.2/local_installers/cuda-repo-ubuntu2004-11-4-local_11.4.2-470.57.02-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu2004-11-4-local_11.2-470.57.02-1_amd64.deb
sudo apt-key add /var/cuda-repo-ubuntu2004-11-4-local/7fa2af80.pub
sudo apt-get update
sudo apt-get -y install cuda

Install CUDA toolkit on Ubuntu 20.04:
sudo apt-get install nvidia-cuda-toolkit

And then you'll need to reboot your system again as installing cuda will most 
likely bring a different set of drivers for your system.

You can then check CUDA is installed correctly by running
nvcc --version
which produces output similar to the following

nvidia-smi

Being correctly installed doesn't necessarily mean CUDA is correctly linked to 
the NVIDIA driver. To make sure this is working, use the bandwidthTest utility 
from CUDA's demo_suite folder, usually found in '/usr/local/cuda-11.4/extras/demo_suite/'.

If everything is working correctly, you should see Result = PASS at the end of 
the output.




### sh: 1: cicc: command not found
sudo apt-get install nvidia-cuda-toolkit 
(but not required when above steps are followed)


--------------------------------------------------------------------------------
When pycuda installed with pip not recognize the cuda-11.4 toolkit, do the 
following to rectify them:

1. add cuda-11.4 toolkit path in .bashrc
export PATH="$PATH:/usr/local/cuda-11.4/bin/"
export PATH="$PATH:/usr/local/cuda-11.4/lib64:/usr/local/cuda-11.4/lib"
then do,
source .bashrc

2. sudo find / -name nvcc
if it shows like,
/usr/local/cuda-11.4/bin/nvcc
/usr/bin/nvcc
then remove the /usr/bin/nvcc by the next step and also
apt-get remove nvidia-cuda-toolkit    
(which is replaced by installing cuda-11.4 by the instructions above the page)

3. undo the following link is done before
ln -s /usr/local/cuda-11.4/bin/nvcc /usr/bin/nvcc 
and to do that simply 
cd /usr/bin
sudo rm nvcc

4. now pycuda should work properly as desired

Reference: 
https://forums.developer.nvidia.com/t/nvcc-preprocessing-failed-compile-error/58422/2
--------------------------------------------------------------------------------






Reference (for installation):
A Complete Introduction to GPU Programming With Practical Examples in CUDA and Python: 
https://www.cherryservers.com/blog/introduction-to-gpu-programming-with-cuda-and-python

GPU data processing inside LXD (4. Install the CUDA toolkit)
https://ubuntu.com/tutorials/gpu-data-processing-inside-lxd#4-install-the-cuda-toolkit

Until you run CuPy (v10.1) on Ubuntu 20.04: 
https://linuxtut.com/en/8e4f627bca01ffe33321/

CuPy Installation:
https://docs.cupy.dev/en/stable/install.html

Basics of CuPy:
https://docs.cupydev/en/stable/user_guide/basic.html



Tutorials:
Programming GPUs with Python: PyOpenCL and PyCUDA
http://homepages.math.uic.edu/~jan/mcs572f16/mcs572notes/lec29.html#programming-guides

CUDA Training series
https://www.olcf.ornl.gov/cuda-training-series/

Installing CuPy
https://docs.olcf.ornl.gov/software/python/cupy.html

PyCuda 2021.1 documentation
https://documen.tician.de/pycuda/tutorial.html

Python pycuda.driver.memcpy_htod() Examples
https://www.programcreek.com/python/example/107750/pycuda.driver.memcpy_htod

OpenCL Exercises
http://soft.web.ac.be/teaching/multicore/opencl/series1_solutions_DEFHD.html

PyOpenCL: Arrays
https://andreask.cs.illinois.edu/tutorial/dsl-tutorial-materials/dist/03-opencl/1-2-pyopencl-arrays.html

Tutorials
https://streamhpc.com/knowledge/for-developers/tutorials/

OpenCL in Action
https://livebook.manning.com/book/opencl-in-action/table-of-contents/

An Introduction to GPU Programming in Julia
https://nextjournal.com/sdanisch/julia-gpu-programming

JuliaGPU
https://juliagpu.org/

Parallel Computing
https://docs.julialang.org/en/v1/manual/parallel-computing/

HPC WIKI GPU Computing (Julia)
https://hpc-wiki.info/hpc/GPU_Tutorial/Julia#quiz1

Video GPU programming with Julia
https://www.cscs.ch/publications/tutorials/2022/video-of-the-course-gpu-programming-with-julia/

Not a Monad Tutorial
https://www.notamonadtutorial.com/julia-gpu/

Julia versus NumPy arrays
http://kylebarbary.com/blog/julia-vs-numpy-arrays/

Intro 3: Basic Concepts in Julia (Part II)
https://people.engr.ncsu.edu/kay/www501S21/1-Intro/1-Intro-3.html

Kernel Tuner
https://benvanwerkhoven.github.io/kernel_tuner/install.html


# cuda gdb (new in 0.94.1) 
$ cuda-gdb --args python -m pycuda.debug program.py
