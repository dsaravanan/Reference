# Octave reference

# installing symbolic package
1. connect to the internet
2. start Gnu Octave
3. on the octave prompt, give the following command
    pkg install -forge symbolic
    this will install the latest symbolic package from octaveforge site
    you can also just download the symbolic package from the site
    https://octave.sourceforge.io/873 then give the command
    pkg install /path_to_the_downloaded_package/symbolic_with_version_number
    you can read more about this in octave-documentation


>> setenv PYTHON /home/saran/.envn/dsci/bin/python3
>> system('curl https://bootstrap.pypa.io/get-pip.py | $PYTHON - --user');
>> system('$PYTHON -m pip install --user sympy')
>> pkg load symbolic
>> syms x
Ref: https://github.com/octave-snap/octave-snap/issues/17
