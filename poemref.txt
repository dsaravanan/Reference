To beauty such as this
    No woman could give birth;
The quivering lightning flash
    Is not a child of earth.


Although she does not speak to me,
    She listens while I speak;
Her eyes turn not to see my face,
    But nothing else they seek.


Her sweetly trembling lip
    With virgin invitation
Provokes my soul to sip
    Delighted fascination.


Be brave, and check the rising tears
    That dim your lovely eyes;
Your feet are stumbling on the path
    That so uneven lies.


A noble husband's honourable wife,
You are to spend a busy, useful life
In the world's eye; and soon, as eastern skies
Bring forth the sun, from you there shall arise
A child, a blessing and a comfort strong
You will not miss me, dearest daughter, long.


The mind of age is like a lamp
    Whose oil is running thin;
One moment it is shining bright,
    Then darkness closes in.


The world you daily guard and bless,
Not heeding pain or weariness;
    Thus is your nature made.
A tree will brave the noonday, when
The sun is fierce, that weary men
    May rest beneath its shade.


Friends come to all whose wealth is sure,
But you, alike to rich and poor,
Are friend both strong and tender.


You are the best of worthy men, they say;
    And she, I know, Good Works personified;
The Creator wrought for ever and a day,
    In wedding such a virtuous groom and bride.


Leave her or take her, as you will;
    She is your wife;
Husbands have power for good or ill
    O'er woman's life.


I cannot sleep at night
    And mmet her dreaming;
I cannot see the sketch
    While tears are streaming. 


Could I forsake the virtuous wife
    Who held my best, my future life
And cherished it for glorious birth,
    As does the seed-receiving earth?


I dare not hope for what I pray;
    Why thrill - in vain?
For heavenly bliss once thrown away
    Turns into pain.


It makes me thrill to touch the boy,
    The stranger's son, to me unknown;
What measureless content must fill
    The man who calls the child his own!


In glittering palaces they dwell
While men, and rule the country well;
Then make the grove their home in age,
And die in austere hermitage.



No means No

Millennia ago, early men assigned
Specific work to women designed
To ease the burden of everyday life
Like care-taking and making knives.

Why does segregation still exist?
Even though we did it all to resist
The inequality between children of god.
But all society did was nod.

You can't go to work, you're a woman;
Your life is destined for the oven.
Men are superior in the eyes of society;
I ask, where's the morality?

Is this why we are looked as objects;
Just a body for men to inject
There's a simple solution, they state
Stay inside your home after eight.

Did she deserved to be groped?
'Oh, let her be safe,' her mother hoped,
But so little did she know
Immoral men didn't let her go.

Teach your daughters to dress
They said, without realising the mess
That was created by their son
For a simple reason: 'just for fun'.

'No means no, let me go,' she said.
Slapped her and pushed her on the bed
For they were tied with a marital bond
Which meant he did no wrong!

'Stop it, please,' she screamed.
But rather, he vigorously leaned
Into her exposed breasts.
Ah well, you know the rest.

Do not worry, for I am 'pure'
I'm just worried that there's no cure
To quench the thirst of men's desires
Who are disguised in various attire.

                - Amrusha Muralidharan


The eight parts of speech of English grammer

Every name is called a [noun],
As [field] and [fountain], [street] and [town].

In place of noun the [pronoun] stands,
As [he] and [she] can clap their hands.

The [adjective] describes a thing,
As [magic] wand or [bridal] ring.

Most [verbs] mean action, something done,
To [read] and [write], to [jump] and [run].

How things are done the [adverbs] tell,
As [quickly], [slowly], [badly], [well].

The [preposition] shows relation,
As [in] the street or [at] the station.

[Conjunctions] join, in many ways,
Sentences, words, [or] phrase [and] phrase.

The [interjection] cries out, ["Heed!]
An exclamation point must follow me!"
