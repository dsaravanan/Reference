Command                     Description
x y moveto                  move current point to (x,y)
x y lineto                  draw a line from current point to (x,y)
x y translate               translate the origin to the point (x,y)
stroke                      "apply ink" to a previously defined path
fill                        fill the interior of a previously defined path
w setlinewidth              set the line width to w
d1 d2 div                   calculate d1/d2: result is placed at top of stack
