virtualization - In computing, virtualization refers to the act of creating a
virtual (rather than physical) version of something, including virtual computer
hardware platforms, storage devices, and computer network resources.

emulator - An emulator is a hardware or software that enables one computer
system (called the host) to behave like another computer system (called the
guest).

virtual machine (VM) - Also known as a "guest machine", these are emulations of
real, physical hardware computers.

hosts - In hardware virtualization, a computer on which a hypervisor runs one or
more VMs.

hypervisor - This is computer software, firmware, or hardware that creates and
runs a VMs.

kernel - This is a computer program at the core of a computer's operating system
with complete control over everything in the system.

daemon - This is a computer program that runs as a background process, rather
than under the direct control of an interactive user.

kernel-based virtual machine (KVM) - A virtualization module in the Linux kernel
that allows the kernel to function as a hypervisor.

quick emulator (QEMU) - A generic and open source machine emulator and virtualizer.

libvirt - A library and daemon providing a stable, open source API for managing
virtualization hosts.

libguestfs - A set of tools for accessing and modifying VM disk images.

virt-manager - A desktop user interface for managing VMs through libvirt.

libosinfo - Provides a database of information about operating system releases
to assist in optimally configuring hardware when deploying VMs.

Reference: https://opensource.com/article/20/8/virt-tools, https://www.virt-tools.org/

How to run DOS programs in Linux with QEMU and FreeDOS
https://opensource.com/article/17/10/run-dos-applications-linux
