Truth, Discipline, Concentration, Knowledge, Courage.

I'm self-motivated, self-disciplined, self-reliance, self-esteem, self-educated,
self-thought/reflection.

Learning is the only thing the mind never exhausts, never fears, and never regrets.
    - Leonardo da Vinci

Of what use is a great civilisational heritage of art and culture when its
bedrock is oppression, bondage, and the treatment of a large section of their
own kind as inferior to the point of subhuman by upper-caste Hindus?
    - Professor Syed Sayeed

The Dunning-Kruger effect, where ignorance masquerades as expertise, must be
replaced by a thirst for real knowledge and humility. Alvin Toffler's wisdom
rings true: "The illiterate of the future are not those who can't read or write
but those who cannot learn, unlearn, and relearn."

Egalitarianism must come before libertarianism or utilitarianism.

Do not seek to follow in the footsteps of the men of old; seek what they sought.
    - Basho

If there are no stupid questions, then what kind of questions do stupid people
ask? Do they get smart just in time to ask questions?
    - Scott Adams

Don't let yesterday take up too much of today.
    - Will Rogers

You'll never understand the wheel unless you re-invent it.

It's important to always ask yourself, how does this library's code work to
solve my porblem?

Research Interests:
Applied Mathematics; Numerical Analysis; Scientific Computing; Stochastic Simulation

We are our habits.

The one who falls and gets up is so much stronger than the one who never fell.

Speak to your children as if they are the wisest, kindest, most beautiful and
magical humans on Earth. For what they BELIEVE is what they will BECOME.
    - Brooke Hampton
