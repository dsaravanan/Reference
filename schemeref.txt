# scheme reference
https://www.scheme.org/
Teach Yourself Scheme in Fixnum Days - https://docs.scheme.org/tyscheme/

# MIT/GNU Scheme

Source file: https://www.gnu.org/software/mit-scheme/
Stable release 12.1
File: Unix binary
Arch: x86-64

Install instructions:
https://www.gnu.org/software/mit-scheme/documentation/stable/mit-scheme-user/Unix-
Installation.html

Requirements:
* Debian-like systems: gcc make m4 libncurses-dev libx11-dev

1. Unpack the tar file, mit-scheme-12.1-x86-64.tar.gz
    tar xzf mit-scheme-12.1-x86-64.tar.gz

2. Move into the src subdirectory of mit-scheme-12.1/
    cd mit-scheme-12.1/src

3. Configure the software (by default, the software will be installed in
   /usr/local, in the subdirectories bin and lib)
    ./configure

4. Build the software:
    make

5. Install the software:
    doas make install

6. Build the documentation:
    cd ../doc
    ./configure
    make

7. Install the documentation:
    doas make install-info install-html install-pdf


# variables
> (define pi 3.14159)
> (define radius 100)
> (define area (* pi radius radius))
> (define circumference (* 2 pi radius))

# functions
> (define (square x) (* x x))
> (square 5)
25
