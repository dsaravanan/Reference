sed (stream editor) reference

GNU sed manual - https://www.gnu.org/software/sed/manual/sed.html
Grymoire sed tutorial - https://www.grymoire.com/Unix/Sed.html

# print only line 45 of the input file
sed -n '45p' input.txt

# prints all input until a line starting with the word 'foo' is found
sed '/^foo/q42' input.txt > output.txt
If such line is found, sed will terminate with exit status 42. If such line was
not found (and no other error occurred), sed will exit with status 0. /^foo/ is
a regular-expression address. q is the quit command. 42 is the command option.

# multiple scripts
Multiple scripts can be specified with -e or -f options.

The following examples are all equivalent. They perform two sed operations: deleting
any lines matching the regular expression /^foo/, and replacing all occurrences of
the string 'regexp' with 'replace'.

sed '/^foo/d ; s/regexp/replace/' input.txt > output.txt
sed -e '/^foo/d' -e 's/regexp/replace/' input.txt > output.txt

echo '/^foo/d' > script.sed
echo 's/regexp/replace/' >> script.sed
sed -f script.sed input.txt > output.txt

echo 's/regexp/replace/' > script.sed
sed -e '/^foo/d' -f script.sed input.txt > output.txt

# find and replace string with sed
sed 's/regexp/replace/g' input.txt # output to the standard output
sed 's/regexp/replace/g' input.txt > output.txt
sed 's/regexp/replace/g' < input.txt > output.txt
cat input.txt | sed 's/regexp/replace/g' - > output.txt
sed -i 's/regexp/replace/g' input.txt # edit file in-place instead of standard output
sed -i.bak 's/regexp/replace/g' input.txt # edit file in-place & also backup input file
sed -i '144s/regexp/replace/g' input.txt # replace regexp only on line 144
sed -i '/berg/s/regexp/replace/g' input.txt # only in lines containing the word berg
sed -i '4,17s/regexp/replace/g' input.txt # replace only in lines 4 to 17 (inclusive)
sed -i '/berg/!s/regexp/replace/g' input.txt # only in lines not containing the berg
sed -i '4,17!s/regexp/replace/g' input.txt # exclude lines 4 to 17

# command to delete all of the material between lines 2206 and 2525
sed '2206,2525d' input.txt > output.txt
sed -i '2206,2525d' input.txt
sed -i.bak '2206,2525d' input.txt

# quit after printing the second line
seq 3 | sed 2q
1
2

# delete the second input line
seq 3 | sed 2d
1
3

# print only the second input line
seq 3 | sed -n 2p
2

# perform substitution on every 3rd line (i.e. two n commands skip two lines)
seq 6 | sed 'n;n;s/./x/'

# transliterate 'a-j' into 0-9
echo hello world | sed 'y/abcdefghij/0123456789/'
74ll0 worl3

# add the word 'string' after the second line
seq 3 | sed '2a string'
1
2
string
3

# demonstrate the step address usage
seq 10 | sed -n '0~4p' # even-numbered lines
4
8

seq 10 | sed -n '1~3p' # odd-number lines
1
4
7
10

# recursive find and replace
recursively search for files in the current working directory and pass the file
names to sed:
find . -type f -exec sed -i 's/regexp/replace/g' {} +

use the -print0 option, to avoid issues with files containing space in their names,
which tells find to print the file name, followed by a null character and pipe the
output to sed using xargs -0:
find . -type f -print0 | xargs -0 sed -i 's/regexp/replace/g'

use -not -path option to exclude a directory:
find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/regexp/replace/g'
For example, if you are replacing a string in your local git repository to exclude
all files starting with dot.

search and replace text only on files with a specific extension:
find . -type f -name "*.md" -print0 | xargs -0 sed -i 's/regexp/replace/g'

recursively find all files containing the search pattern and then pipe the
filenames to sed:
grep -rlZ 'regexp' . | xargs -0 sed -i.bak 's/regexp/replace/g'
