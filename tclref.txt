$ tclsh    # Tcl interpreter

% set institute "Institute of Mathematical Sciences"
% puts "Institute of Mathematical Sciences"
Institute of Mathematical Sciences
% puts "The institution name is $institute"
The institution name is Institute of Mathematical Sciences

% expr sin(4)
-0.7568024953079282

% set total 1
% set count 10
% puts $count
10
% while {$count > 0} {
    set total [expr $total * $count]
    set count [expr $count - 1]
}
% puts $count
0

# the two commands are equivalent
% set count [expr $count - 1]
% incr count -1 # with no other argument incr adds one

% exit
