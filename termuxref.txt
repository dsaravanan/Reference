# Termux reference

Termux is a terminal emulator application for Android OS which can be extended
by packages of ported common GNU/Linux utilities.

Home page: https://termux.dev
Github: https://github.com/termux
Termux Packages: https://packages.termux.dev/

There are hosted apt repositories for the Termux project.

Repository      sources.list entry
Main            deb https://packages.termux.dev/apt/termux-main/ stable main
Root            deb https://packages.termux.dev/apt/termux-root/ root stable
X11             deb https://packages.termux.dev/apt/termux-x11/ x11 main

u0_a74

# installation of root repository
pkg install root-repo

# installation of unstable repository
pkg install unstable-repo

# installation of x11 repository
pkg install x11-repo

# setup storage
termux-setup-storage

# installing Python
pkg install python

# installing wget
pkg install wget

# installing zip
pkg install zip

# check ip address in termux: (check for 'UP')
ip -br addr | grep -v DOWN
sshd    # start ssh demeon in termux

# ssh to termux:
ssh -p 8022 u0_a499@192.168.43.128    [5Tw@4rEWn3]
scp -P 8022 file.tar.gz u0_a499@192.168.43.128:~/Tardirectory/
scp -v -P 8022 file.tar.gz u0_a499@192.168.43.128:~/Tardirectory/

ssh -p 8022 u0_a275@192.168.29.121 # realme

# remove welcome screen text in termux
Open termux app and type:
cd ../usr/etc/
rm motd

devscripts

lambdalabs.com
dayalsaravanan@gmail.com
@veish7Sh!
