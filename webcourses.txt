Courses on the Web

# Modules on Programming Methodology, Programming Abstractions, and Programming Paradigms
Stanford Engineering Everywhere - http://see.stanford.edu/

# artificial intelligence - https://www.ai-class.com/
# factuly members - Sebastian Thrun and Peter Norvig
# course material - https://www.ai-class.com/overview

# build search engines and web application engineering
http://www.udacity.com/

# Coursera - https://www.coursera.org/

# 20 Things I Learned about Browsers and the Web - http://www.20thingsilearned.com

# Moodle - http://moodle.org/
# MoodleShare - http://courses.moodleshare.com/
# Chemistry 101 Course - http://courses.moodleshare.com/course/view.php?id=160

# get download courses - https://sites.google.com/site/moodlemayhem/moodle-courses
# Free student courses - http://www.freestudentcourses.co.uk/
# free downloadable Moodle courses

# remote desktop sharing
